(function() {
    'use strict';

    angular
        .module('starter', [
            'ngSanitize',
            'ui.router',
            'ui.router.state.events',
            'ui.bootstrap',
            'restangular',
            'angularMoment',
            'LocalStorageModule',
            'toastr',
            'ngDialog',
            'ngFileUpload',
            'angular-jwt',
            'thatisuday.dropzone',
            'angular-loading-bar'
        ])
        .constant('API_URL', 'http://localhost:3001')
        .constant('API_URL_PROD', 'https://shoppy-123456789.herokuapp.com')
        .constant('API_VERSION', '/app/1/')
        .constant('isPRODUCTION', false)
        .constant('_', window._)
        .constant('moment', window.moment)
        .constant('async', window.async)
        .constant('EVAL_CATEGORY',{
            applicants: 'APPLICANTS',
            inReview: 'IN-REVIEW',
            forApproval: 'FOR-APPROVAL',
            completed: 'COMPLETED'
        })
        .config(config);

    config.$inject = [
        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider',
        '$httpProvider',
        'RestangularProvider',
        'API_URL',
        'API_URL_PROD',
        'API_VERSION',
        'isPRODUCTION',
        'localStorageServiceProvider'
    ];

    function config(
        $stateProvider,
        $urlRouterProvider,
        $locationProvider,
        $httpProvider,
        RestangularProvider,
        API_URL,
        API_URL_PROD,
        API_VERSION,
        isPRODUCTION,
        localStorageServiceProvider
    ) {
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });
        $locationProvider.hashPrefix('');

        $urlRouterProvider.otherwise('/officialshop/home');

        localStorageServiceProvider.setPrefix('shoppy-ofc');
        var baseUrl = (isPRODUCTION === true ? API_URL_PROD : API_URL) + API_VERSION;
        RestangularProvider.setBaseUrl(baseUrl);

        $stateProvider
            .state('officialshop', {
                abstract: true,
                url: '/officialshop',
                templateUrl: 'modules/application.html'
            })
            .state('officialshop.home', {
                url: '/home',
                templateUrl: 'modules/intro.html',
                controller: 'applicationOfficialHomeCtrl'
            })
            .state('officialshop.intro', {
                url: '/intro?code&token&action',
                templateUrl: 'modules/application.intro.html',
                data: {
                    pageTitle: 'Official Shops',
                    specialClass: 'gray-bg'
                },
                controller: 'applicationOfficialIntroCtrl'
            })
            .state('officialshop.main', {
                url: '/main?code&token&action',
                templateUrl: 'modules/application.main.html',
                data: {
                    pageTitle: 'Official Shops',
                    specialClass: 'gray-bg'
                },
                controller: 'applicationOfficialShopCtrl'
            })
            .state('officialshop.step_one', {
                url: '/step_one?code&token&action',
                templateUrl: 'modules/wizard/step_one.html',
                data: {
                    pageTitle: 'Application Form'
                },
                controller: 'applicationOfficialShopOneCtrl'
            })
            .state('officialshop.step_two', {
                url: '/step_two?code&token&action',
                templateUrl: 'modules/wizard/step_two.html',
                data: {
                    pageTitle: 'Application Form'
                },
                controller: 'applicationOfficialShopTwoCtrl'
            })
            .state('officialshop.status', {
                url: '/status?code&token&action',
                templateUrl: 'modules/wizard/step_three.html',
                data: {
                    pageTitle: 'Application Form'
                },
                controller: 'applicationStatusCtrl'
            })
            .state('officialshop.success', {
                url: '/success',
                templateUrl: 'modules/wizard/success.html',
                data: {
                    pageTitle: 'Official Shops',
                    specialClass: 'gray-bg'
                }
            });
    }
})();