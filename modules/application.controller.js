(function() {
    'use strict';

    angular.module('starter')
        .controller('applicationOfficialShopOneCtrl', applicationOfficialShopOneCtrl)
        .controller('applicationOfficialShopTwoCtrl', applicationOfficialShopTwoCtrl);


    applicationOfficialShopOneCtrl.$inject = ['_', 'async', '$scope', '$state', '$stateParams', 'toastr', '$rootScope', 'shopsFctry', '$window', 'ngDialog', 'API_URL', 'API_URL_PROD', 'API_VERSION', 'isPRODUCTION', 'Upload'];

    function applicationOfficialShopOneCtrl(_, async, $scope, $state, $stateParams, toastr, $rootScope, shopsFctry, $window, ngDialog, API_URL, API_URL_PROD, API_VERSION, isPRODUCTION, Upload) {
        console.log('applicationOfficialShopOneCtrl');

        $scope.formData = {};
        $scope.withImage = false;
        $scope.isSaving = false;
        $scope.isValidOne = false;

        var sellerInfo = JSON.parse(localStorage.getItem('seller.shop')) || {};
        var formData1 = JSON.parse(localStorage.getItem('application.step_two')) || {};
        if (!_.isEmpty(formData1)) {
            $scope.formData = formData1;
        } else {
            shopsFctry.getApplication({
                code: $stateParams.code || sellerInfo.shop_url_code,
                token: $stateParams.token || sellerInfo.shop_url_token,
            }, sellerInfo.shop_url_token).then(function(data) {
                console.log('data: ', data.result);
                if (data && data.success) {
                    $scope.formData = data.result;
                    $scope.logoFile = data.result.shop_official_logo;
                }
            }, function(error) {
                console.log('error: ', error);
            });
        }

        $scope.back = function() {
            $window.history.back();
        };

        $scope.upload = function(file) {
            if (file) {
                $scope.withImage = true;
            } else {
                $scope.withImage = false;
            }
        };

        $scope.processForm = function() {
            var submitForm = function() {
                if (_.isEmpty($scope.formData)) {
                    if ($scope.submitForm && $scope.submitForm.$error) {
                        angular.forEach($scope.submitForm.$error.required, function(field) {
                            field.$setDirty();
                        });
                    }
                    toastr.warning('Please fill out the required fields', 'WARNING');
                    $scope.isValidOne = false;
                    console.log('$scope.isValidOne: ', $scope.isValidOne);
                    return;
                }

                if ($scope.submitForm.shop_name.$invalid) {
                    $scope.isValidOne = false;
                    return;
                } else if ($scope.submitForm.shop_email.$invalid) {
                    $scope.isValidOne = false;
                    return;
                } else if ($scope.submitForm.shop_mobileno.$invalid) {
                    $scope.isValidOne = false;
                    return;
                } else if ($scope.submitForm.owner_fname.$invalid) {
                    $scope.isValidOne = false;
                    return;
                } else if ($scope.submitForm.owner_mname.$invalid) {
                    $scope.isValidOne = false;
                    return;
                } else if ($scope.submitForm.owner_lname.$invalid) {
                    $scope.isValidOne = false;
                    return;
                } else if ($scope.submitForm.owner_email.$invalid) {
                    $scope.isValidOne = false;
                    return;
                } else {
                    $scope.isValidOne = true;
                }

                $scope.formData.code = $stateParams.code || sellerInfo.shop_url_code;
                $scope.formData.userId = sellerInfo.userId;

                if (!$scope.submitForm.$valid) {
                    toastr.warning('Please fill up all the required fields', 'WARNING');
                    return;
                }

                if ($scope.isValidOne) {
                    localStorage.setItem('application.step_two', JSON.stringify($scope.formData));

                    async.waterfall([
                        function(callback) {
                            $scope.isSaving = true;
                            shopsFctry.saveApplication($scope.formData, sellerInfo.shop_url_token).then(function(data) {
                                console.log('data: ', data);
                                if (data && data.success) {
                                    toastr.success(data.msg, 'SUCCESS');
                                    callback();
                                } else {
                                    toastr.warning(data.msg, 'WARNING');
                                    return;
                                }
                            }, function(error) {
                                console.log('error: ', error);
                            });
                        },
                        function(callback) {
                            if ($scope.withImage) {
                                var baseUrl = (isPRODUCTION === true ? API_URL_PROD : API_URL) + API_VERSION + 'officialshop/upload/logo';
                                Upload.upload({
                                    url: baseUrl,
                                    data: {
                                        logo: $scope.logoFile,
                                        'urlcode': sellerInfo.shop_url_code,
                                        'refcode': sellerInfo.shop_ref_code,
                                        'userId': sellerInfo.userId
                                    },
                                    headers: {
                                        'auth_token': sellerInfo.shop_url_token
                                    }
                                }).then(function(resp) {
                                    console.log('Success Response: ', resp.data);
                                    callback();
                                }, function(resp) {
                                    console.log('Error status: ' + resp.status);
                                }, function(evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    console.log('progress: ' + progressPercentage + '% ');
                                });
                            } else {
                                callback();
                            }
                        }
                    ], function() {
                        $scope.isSaving = false;
                        localStorage.removeItem('application.step_two');
                        $window.location.reload();
                    });
                } else {
                    toastr.warning('Please fill up all the required fields', 'WARNING');
                    return;
                }
            };

            $scope.modal = {
                title: 'Save Entry',
                message: 'Are you sure to submit your entry?'
            };

            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                submitForm();
            }, function() {});
        };
    }



    applicationOfficialShopTwoCtrl.$inject = ['_', 'async', '$scope', '$state', '$stateParams', 'toastr', '$rootScope', 'shopsFctry', '$window', 'ngDialog', 'API_URL', 'API_URL_PROD', 'API_VERSION', 'isPRODUCTION', 'Upload'];

    function applicationOfficialShopTwoCtrl(_, async, $scope, $state, $stateParams, toastr, $rootScope, shopsFctry, $window, ngDialog, API_URL, API_URL_PROD, API_VERSION, isPRODUCTION, Upload) {
        $scope.formData = {};
        $scope.isValidOne = false;
        $scope.withImage = false;
        $scope.withCOR = false;
        $scope.withLicense = false;
        $scope.isSaving = false;

        $scope.CORFiles = [];
        $scope.LicenseFiles = [];

        $scope.CORFilename = '';
        $scope.LicenseFilename = '';


        $scope.filePattern = 'application/pdf, application/octet-stream, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document';

        var sellerInfo = JSON.parse(localStorage.getItem('seller.shop')) || {};

        shopsFctry.getApplication({
            code: $stateParams.code || sellerInfo.shop_url_code,
            token: $stateParams.token || sellerInfo.shop_url_token,
        }, sellerInfo.shop_url_token).then(function(data) {
            console.log('data: ', data.result);
            if (data && data.success) {
                $scope.formData = data.result;

                if ($scope.formData.shop_cor_filename) {
                    var CORFilename = ($scope.formData.shop_cor_filename).split('/');
                    $scope.CORFilename = CORFilename[_.size(CORFilename) - 1];
                }

                if ($scope.formData.shop_cor_filename) {
                    var LicenseFilename = ($scope.formData.shop_cor_filename).split('/');
                    $scope.LicenseFilename = LicenseFilename[_.size(LicenseFilename) - 1];
                }
            }
        }, function(error) {
            console.log('error: ', error);
        });

        $scope.upload = function(file) {
            if (file) {
                $scope.withImage = true;
            } else {
                $scope.withImage = false;
            }
        };

        $scope.uploadCORFile = function(file) {
            if (file) {
                $scope.withCOR = true;
            } else {
                $scope.withCOR = false;
            }
            $scope.CORFiles = file;
        };

        $scope.uploadLicenseFile = function(file) {
            if (file) {
                $scope.withLicense = true;
            } else {
                $scope.withLicense = false;
            }
            $scope.LicenseFiles = file;
        };

        $scope.changeCORFile = function() {
            $scope.CORFiles = [];
            $scope.withCOR = false;
        };

        $scope.changeLicenseFile = function() {
            $scope.LicenseFiles = [];
            $scope.withLicense = false;
        };


        $scope.back = function() {
            $window.history.back();
        };

        $scope.deleteCORFile = function() {
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                shopsFctry.deleteCORFile({
                    code: $stateParams.code || sellerInfo.shop_url_code,
                    ref_code: sellerInfo.shop_ref_code,
                    userId: sellerInfo.userId
                }, sellerInfo.shop_url_token).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'SUCCESS');
                        /*setTimeout(function(){
                            $window.location.reload();
                        },1000);*/
                    } else {
                        toastr.warning(data.msg, 'WARNING');
                    }
                }, function(error) {
                    console.log('Error: ', error);
                });
            }, function() {});
        };

        $scope.deleteBusinessFile = function() {
            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/delete.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                shopsFctry.deleteBusinessLicenseFile({
                    code: $stateParams.code || sellerInfo.shop_url_code,
                    ref_code: sellerInfo.shop_ref_code,
                    userId: sellerInfo.userId
                }, sellerInfo.shop_url_token).then(function(data) {
                    if (data && data.success) {
                        toastr.success(data.msg, 'SUCCESS');
                        setTimeout(function() {
                            $window.location.reload();
                        }, 1000);
                    } else {
                        toastr.warning(data.msg, 'WARNING');
                    }
                }, function(error) {
                    console.log('Error: ', error);
                });
            }, function() {});
        };


        $scope.processForm = function() {
            console.log('$scope.formData: ', $scope.formData);

            var submitForm = function() {
                if (_.isEmpty($scope.formData)) {
                    if ($scope.submitForm && $scope.submitForm.$error) {
                        angular.forEach($scope.submitForm.$error.required, function(field) {
                            field.$setDirty();
                        });
                    }

                    toastr.warning('Please fill out the required fields', 'WARNING');
                    $scope.isValidOne = false;
                    return;
                }

                if ($scope.submitForm.shop_cor_number.$invalid) {
                    $scope.isValidOne = false;
                    toastr.warning('Please fill up Shop COR Number', 'WARNING');
                    return;
                } else if ($scope.submitForm.shop_distributor_license_number.$invalid) {
                    toastr.warning('Please fill up Shop License Number', 'WARNING');
                    $scope.isValidOne = false;
                    return;
                } else {
                    $scope.isValidOne = true;
                }

                $scope.formData.code = $stateParams.code || sellerInfo.shop_url_code;
                $scope.formData.userId = sellerInfo.userId;

                if (!$scope.submitForm.$valid) {
                    toastr.warning('Please fill up all the required fields', 'WARNING');
                    return;
                }

                if ($scope.isValidOne) {
                    async.parallel([
                        function(callback) {
                            $scope.isSaving = true;
                            if ($scope.withImage) {
                                var baseUrl = (isPRODUCTION === true ? API_URL_PROD : API_URL) + API_VERSION + 'officialshop/upload/logo';
                                Upload.upload({
                                    url: baseUrl,
                                    data: {
                                        logo: $scope.logoFile,
                                        'urlcode': sellerInfo.shop_url_code,
                                        'refcode': sellerInfo.shop_ref_code,
                                        'userId': sellerInfo.userId
                                    },
                                    headers: {
                                        'auth_token': sellerInfo.shop_url_token
                                    }
                                }).then(function(resp) {
                                    console.log('Success Response: ', resp.data);
                                    callback();
                                }, function(resp) {
                                    console.log('Error status: ' + resp.status);
                                }, function(evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    console.log('progress: ' + progressPercentage + '% ');
                                });
                            } else {
                                callback();
                            }
                        },
                        function(callback) {
                            if ($scope.withCOR) {
                                var baseUrl = (isPRODUCTION === true ? API_URL_PROD : API_URL) + API_VERSION + 'officialshop/upload/cor';
                                Upload.upload({
                                    url: baseUrl,
                                    data: {
                                        file: $scope.CORFiles[0],
                                        'shop_cor_number': $scope.formData.shop_cor_number,
                                        'urlcode': sellerInfo.shop_url_code,
                                        'refcode': sellerInfo.shop_ref_code,
                                        'userId': sellerInfo.userId
                                    },
                                    headers: {
                                        'auth_token': sellerInfo.shop_url_token
                                    }
                                }).then(function(resp) {
                                    console.log('Success Response: ', resp.data);
                                    callback();
                                }, function(resp) {
                                    console.log('Error response: ' + resp.data);
                                }, function(evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    console.log('progress: ' + progressPercentage + '% ');
                                });
                            } else {
                                callback();
                            }
                        },
                        function(callback) {
                            if ($scope.withLicense) {
                                var baseUrl = (isPRODUCTION === true ? API_URL_PROD : API_URL) + API_VERSION + 'officialshop/upload/license';
                                Upload.upload({
                                    url: baseUrl,
                                    data: {
                                        file: $scope.LicenseFiles[0],
                                        'shop_distributor_license_number': $scope.formData.shop_distributor_license_number,
                                        'urlcode': sellerInfo.shop_url_code,
                                        'refcode': sellerInfo.shop_ref_code,
                                        'userId': sellerInfo.userId
                                    },
                                    headers: {
                                        'auth_token': sellerInfo.shop_url_token
                                    }
                                }).then(function(resp) {
                                    console.log('Success Response: ', resp.data);
                                    callback();
                                }, function(resp) {
                                    console.log('Error status: ' + resp.status);
                                }, function(evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    console.log('progress: ' + progressPercentage + '% ');
                                });
                            } else {
                                callback();
                            }
                        }
                    ], function() {
                        $scope.isSaving = false;
                        localStorage.removeItem('application.step_three');
                        $window.location.reload();
                    });
                } else {
                    toastr.warning('Please fill up all the required fields', 'WARNING');
                    return;
                }
            };

            $scope.modal = {
                title: 'Save Entry',
                message: 'Are you sure to submit your entry?'
            };

            ngDialog.openConfirm({
                templateUrl: './modules/dialogs/custom.dialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                submitForm();
            }, function() {});
        };

    }
})();