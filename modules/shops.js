(function() {
    'use strict';

    angular.module('starter')
        .factory('shopsFctry', shopsFctry);

    shopsFctry.$inject = ['Restangular'];

    function shopsFctry(Restangular) {
        return {
            validateCode: function(data) {
                return Restangular.all('officialshop/validate').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            saveApplication: function(data, token) {
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setDefaultHeaders({
                        'auth_token': token
                    });
                }).all('officialshop/info').customPOST(data).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            getApplication: function(params, token) {
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setDefaultHeaders({
                        'auth_token': token
                    });
                }).all('officialshop/info').customGET('', params).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteCORFile: function(params, token) {
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setDefaultHeaders({
                        'auth_token': token
                    });
                }).all('officialshop/upload/cor').customDELETE('', params).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteBusinessLicenseFile: function(params,token){
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setDefaultHeaders({
                        'auth_token': token
                    });
                }).all('officialshop/upload/license').customDELETE('',params).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            },
            deleteBusinessLogo: function(params, token){
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setDefaultHeaders({
                        'auth_token': token
                    });
                }).all('officialshop/upload/logo').customDELETE('',params).then(function(res) {
                        return res;
                    },
                    function(err) {
                        return err.data;
                    });
            }
        };
    }
})();