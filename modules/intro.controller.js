(function() {
    'use strict';

    angular.module('starter')
        .controller('applicationOfficialHomeCtrl', applicationOfficialHomeCtrl)
        .controller('applicationOfficialIntroCtrl', applicationOfficialIntroCtrl)
        .controller('applicationStatusCtrl', applicationStatusCtrl);


    applicationOfficialHomeCtrl.$inject = ['_', '$scope', 'shopsFctry', 'toastr', '$state'];

    function applicationOfficialHomeCtrl(_, $scope, shopsFctry, toastr, $state) {
        $scope.seller = {};

        $scope.validateCode = function() {
            console.log('seller: ', $scope.seller);
            shopsFctry.validateCode($scope.seller).then(function(data) {
                if (data && data.success) {
                    toastr.success(data.msg, 'SUCCESS');
                    localStorage.setItem('seller.shop', JSON.stringify(data.result));
                    setTimeout(function() {
                        $state.go('officialshop.intro', {
                            code: data.result.shop_url_code,
                            token: data.result.shop_url_token,
                            action: data.result.action
                        });
                    }, 1000);
                } else if (!data.success && _.isArray(data.result)) {
                    _.each(data.result, function(msg) {
                        toastr.warning(msg.msg, 'WARNING');
                    });
                } else {
                    toastr.error(data.msg, 'Error');
                }
            }, function(error) {
                console.log('error: ', error);
            });
        };
    }


    applicationOfficialIntroCtrl.$inject = ['$scope', '$stateParams', '$state'];

    function applicationOfficialIntroCtrl($scope, $stateParams, $state) {

        console.log('$stateParams: ', $stateParams);

        $scope.submitShopInfo = function() {
            console.log('submitShopInfo');
            $state.go('officialshop.step_one', {
                code: $stateParams.code,
                token: $stateParams.token,
                action: $stateParams.action
            });
        };

        $scope.submitShopLegalInfo = function() {
            console.log('submitShopLegalInfo');
            $state.go('officialshop.step_two', {
                code: $stateParams.code,
                token: $stateParams.token,
                action: $stateParams.action
            });
        };

        $scope.checkShopStatus = function() {
            console.log('checkShopStatus');
            $state.go('officialshop.status', {
                code: $stateParams.code,
                token: $stateParams.token,
                action: $stateParams.action
            });
        };
    }

    applicationStatusCtrl.$inject = ['_', '$scope', '$stateParams', '$state', '$window', 'shopsFctry', 'EVAL_CATEGORY'];

    function applicationStatusCtrl(_, $scope, $stateParams, $state, $window, shopsFctry, EVAL_CATEGORY) {
        var sellerInfo = JSON.parse(localStorage.getItem('seller.shop')) || {};
        $scope.formData = {};
        $scope.CORFiles = [];
        $scope.LicenseFiles = [];

        $scope.CORFilename = '';
        $scope.LicenseFilename = '';
        $scope.LogoFile = '';

        $scope.progressCount = 0;

        $scope.back = function() {
            $window.history.back();
        };

        shopsFctry.getApplication({
            code: $stateParams.code || sellerInfo.shop_url_code,
            token: $stateParams.token || sellerInfo.shop_url_token,
        }, sellerInfo.shop_url_token).then(function(data) {
            console.log('data: ', data.result);
            if (data && data.success) {
                $scope.formData = data.result;

                if ($scope.formData.shop_cor_filename) {
                    var CORFilename = ($scope.formData.shop_cor_filename).split('/');
                    $scope.CORFilename = CORFilename[_.size(CORFilename) - 1];
                }

                if ($scope.formData.shop_cor_filename) {
                    var LicenseFilename = ($scope.formData.shop_cor_filename).split('/');
                    $scope.LicenseFilename = LicenseFilename[_.size(LicenseFilename) - 1];
                }

                if ($scope.formData.shop_official_logo_filename) {
                    var LogoFile = ($scope.formData.shop_official_logo_filename).split('/');
                    $scope.LogoFile = LogoFile[_.size(LogoFile) - 1];
                }

                if (!_.isEmpty($scope.formData.activity)) {
                    if (_.find($scope.formData.activity, {
                            'eval_categoryID': EVAL_CATEGORY.applicants
                        })) {
                        $scope.progressCount += 25;
                    }

                    if (_.find($scope.formData.activity, {
                            'eval_categoryID': EVAL_CATEGORY.inReview
                        })) {
                        $scope.progressCount += 25;
                    }

                    if (_.find($scope.formData.activity, {
                            'eval_categoryID': EVAL_CATEGORY.forApproval
                        })) {
                        $scope.progressCount += 25;
                    }

                    if (_.find($scope.formData.activity, {
                            'eval_categoryID': EVAL_CATEGORY.completed
                        })) {
                        $scope.progressCount += 25;
                    }
                } else {
                    $scope.progressCount = 0;
                }
                console.log('progressCount: ', $scope.progressCount);
            }
        }, function(error) {
            console.log('error: ', error);
        });
    }
})();